import React from "react";
import { GoogleMap, Marker, withGoogleMap, withScriptjs } from "react-google-maps";

const Map = props => {
  return (
    <GoogleMap defaultZoom={18} defaultCenter={{ lat: -34.603713585863204, lng: -58.38157167933714 }}>
      {props.isMarkerShown && (
        <Marker position={{ lat: -34.397, lng: 150.644 }} />
      )}
    </GoogleMap>
  );
};

export default withScriptjs(withGoogleMap(Map));
