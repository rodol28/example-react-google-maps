import React from "react";
import styled from "styled-components";
import { credentials } from "../../credentials";
import Map from "../Map";
import { ContainerElement } from "../ContainerElement";
import { Spinner } from "@chakra-ui/react";

const mapURL = `https://maps.googleapis.com/maps/api/js?key=${credentials.mapsKey}&v=3.exp&libraries=geometry,drawing,places`;

const Container = styled.div`
  margin: ${props => props.marginx || "100px"}
    ${props => props.marginy || "100px"};
`;

export function MapContainer({
  initialCenter,
  zoom,
  isMarkerShown,
  width,
  height,
  marginx,
  marginy,
}) {
  return (
    <Container marginx={marginx} marginy={marginy}>
      <Map
        googleMapURL={mapURL}
        containerElement={<ContainerElement width={width} height={height} />}
        mapElement={<div style={{ height: `100%` }} />}
        loadingElement={<Spinner />}
        initialCenter={initialCenter}
        zoom={zoom}
        isMarkerShown={isMarkerShown}
      />
    </Container>
  );
}
