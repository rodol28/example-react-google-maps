import React from "react";
import styled from "styled-components";

const Container = styled.div`
  width: ${props => props.width || "300px"};
  height: ${props => props.height || "300px"};
`;

export function ContainerElement({ width, height, children }) {
  return (
    <Container width={width} height={height}>
      {children}
    </Container>
  );
}
