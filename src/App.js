import React, { useState } from "react";
import "./App.css";
import { MapContainer } from "./components/MapContainer";

const initialCenter = { lat: -34.60363147563426, lng: -58.381549054603276 };

function App() {
  // eslint-disable-next-line
  const [center, setCenter] = useState(initialCenter);

  return (
    <div className="App">
      <MapContainer
        initialCenter={center}
        zoom={18}
        isMarkerShown={true}
        width={"100%"}
        height={"600px"}
        marginx={"200px"}
        marginy={"100px"}
      />
    </div>
  );
}

export default App;
